import React, {useState} from 'react';
import {Button, DatePicker, Form, Input, Modal, Select} from "antd";

function CreateDeliveryModal(props) {
  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const {trucks} = props;
  const { RangePicker } = DatePicker;
  const truckList = [
    { label: 'truck1', value: 'truck1' },
    { label: 'truck2', value: 'truck2' },
    { label: 'truck3', value: 'truck3' },
  ];

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };


  const [componentSize, setComponentSize] = useState('default');

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Create Delivery
      </Button>
      <Modal
        title="Title"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Form
          labelCol={{
            span: 7,
          }}
          wrapperCol={{
            span: 22,
          }}
          layout="horizontal"
          initialValues={{
            size: componentSize,
          }}
          onValuesChange={onFormLayoutChange}
          size={componentSize}
        >
        
          <Form.Item label="Truck">
            <Select options={truckList}>
              {/* {
                trucks.map(truck => {
                  return <Select.option value={truck.truckId}> {truck.truckId} </Select.option>
                })
              } */}
            </Select>
          </Form.Item>          
          <Form.Item label="DateTimePicker">
            <RangePicker showTime format="YYYY-MM-DD HH:mm" />
          </Form.Item>
          <Form.Item name="address" label="Delivery Address" rules={[{ required: true }]}>
            <Input />
          </Form.Item>          
        </Form>
      </Modal>
    </>
  );
}
export default CreateDeliveryModal;
