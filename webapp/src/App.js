import 'antd/dist/antd.css';
import {Button, Card, Col, Layout, Row, Table} from "antd";
import {Content, Footer, Header} from "antd/es/layout/layout";
import {useCallback, useEffect, useState} from "react";
import CreateDeliveryModal from "./CreateDeliveryModal";
import Title from "antd/lib/typography/Title";
import UpdateDeliveryModal from "./UpdateDeliveryModal";
import {getTrucks, getDeliveries} from "./api";

const trucksInit = [];
const deliveriesInit = [];

const columns = [
  {
    title: 'Name',
    dataIndex: 'deliveryName',
    key: 'deliveryName',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Truck',
    dataIndex: 'truckId',
    key: 'truckId',
  },
  {
    title: 'Start Date & Time',
    dataIndex: 'deliveryStartDateTime',
    key: 'deliveryStartDateTime',
  },
  {
    title: 'End Date & Time',
    dataIndex: 'deliveryEndDateTime',
    key: 'deliveryEndDateTime',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status'
  },
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: action => <UpdateDeliveryModal />
  },
];


function App() {
  const [deliveries, setDeliveries] = useState(deliveriesInit)
  const [trucks, setTrucks] = useState(trucksInit)
  const fetchTrucks = useCallback(async () => {
    const trucks = await getTrucks();
    const deliveries = await getDeliveries();
    console.log(trucks);
    console.log(deliveries);
    setTrucks(trucks)
    setDeliveries(deliveries);
  }, [])
  useEffect(() => {
    fetchTrucks().then(() => console.log("fetchTrucks is called"));
  }, [fetchTrucks])
  return (
    <Layout>
      <Header style={{position: 'fixed', zIndex: 1, width: '100%'}}>
        <div className="logo" style={{color: "#FFF"}}>
          Truck Delivery System
        </div>
      </Header>
      <Content className="site-layout" style={{padding: '0 50px', marginTop: 64}}>
        <div className="site-layout-background" style={{padding: 24, minHeight: 380}}>          
          <Row gutter={16} justify={"end"}>            
            <CreateDeliveryModal trucks={trucks} />
          </Row>
          <Title level={2}>
            List of Deliveries
          </Title>
          <Row gutter={16}>
            <Table dataSource={deliveries} columns={columns} style={{width: "100%"}} />;
          </Row>
          <Title level={2}>
            List of Trucks
          </Title>
          <Row gutter={16}>
            {
              trucks.map(truck => {
                return <Col span={6} id={truck.truckId}>
                  <Card title={truck.truckId} bordered={false}>
                    <Button>
                      Check Availability
                    </Button>
                  </Card>
                </Col>
              })
            }
          </Row>
        </div>
      </Content>
      <Footer style={{textAlign: 'center'}}>Truck Scheduling system</Footer>
    </Layout>
  );
}

export default App;
