# Truck Delivery Scheduling System
This project is a truck delivery scheduling system.
- User could view the list of deliveries and trucks. 
- User could create new delivery only when the truck is available at the defined delivery date time.
- User could update the delivery status of each delivery. Status as below:
    - `NEW, SCHEDULED, COMPLETED, CANCELLED`

There are 2 parts
1. server - consist of APIs for System dashboard to call.
2. webapp - System dashboard.

Run both server and webapp.

Go to browser url that was defined in webapp