var truckService = require('../service/TruckService')

exports.getTrucks = (req, res) => {
    return truckService.getTrucks(req, res);
 };

 exports.getSingleTruck = (req, res) => {
    return truckService.getSingleTruck(req, res);
 };