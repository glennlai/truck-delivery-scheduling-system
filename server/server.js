const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const myRouter = require('./router/myRouter')
const TruckSchema = require('./model/entity/Truck')

// const mongoClient = require('mongodb').MongoClient;
// const url = "mongodb://localhost/mydb";
// mongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   var dbo = db.db("mydb");
//   dbo.createCollection("deliveries", function(err, res) {
//     if (err) throw err;
//     console.log("Collection deliveries created!");
//     db.close();
//   });
//   dbo.createCollection("trucks", function(err, res) {
//     if (err) throw err;
//     console.log("Collection trucks created!");
//     db.close();
//   });
// });
mongoose.connect('mongodb://localhost/mydb', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
.then(() => console.log('Successfully connect to mongo'))
.catch(err => console.error('Connection error', err));


const truck1 = new TruckSchema({truckId: 'truck1', truckName: 'truckOne'});
const truck2 = new TruckSchema({truckId: 'truck2', truckName: 'truckTwo'});
const truck3 = new TruckSchema({truckId: 'truck3', truckName: 'truckThree'});

TruckSchema.remove().exec();
TruckSchema.insertMany([truck1, truck2, truck3]
    ).then(function(){
        console.log("Data inserted")  // Success
    }).catch(function(error){
        console.log(error)      // Failure
    });

// define our app using express
const app = express();                 

app.use(cors())

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/v1/api', myRouter);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Application started on port ' + port);