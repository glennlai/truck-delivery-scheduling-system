const mongoose = require('mongoose');

// define delivery schema
const deliverySchema = new mongoose.Schema({
        deliveryId : { type: String, required: true },
        deliveryName: { type: String, required: true },
        address: { type: String, required: true },
        startDateTime: { type: String, required: true },
        endDateTime: { type: String, required: true },
        deliveryStatus: { type: String, required: true },
        truckId: { type: String, required: true },
    });

module.exports = mongoose.model('deliveryModel', deliverySchema, 'deliveries');

