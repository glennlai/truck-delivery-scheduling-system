const mongoose = require('mongoose');

// define truck schema
const truckSchema = new mongoose.Schema({
        truckId : { type: String, required: true },
        truckName: { type: String, required: true }
    });

// export schema to model
module.exports = mongoose.model('truckModel', truckSchema, 'trucks');
