class UpdateStatusResponse {
    constructor(deliveryId, updatedStatus) {
        this.deliveryId = deliveryId;
        this.updatedStatus = updatedStatus;
      }
}

module.exports = UpdateStatusResponse;