class GetDeliveriesResponse {
    constructor(deliveryId, deliveryName, truckId, address, deliveryStartDateTime, deliveryEndDateTime, status) {
        this.deliveryId = deliveryId;
        this.deliveryName = deliveryName;
        this.truckId = truckId;    
        this.address = address;
        this.deliveryStartDateTime = deliveryStartDateTime;
        this.deliveryEndDateTime = deliveryEndDateTime;
        this.status = status;
      }
}

module.exports = GetDeliveriesResponse;