class GetTrucksResponse {
    constructor(truckId, truckName) {
        this.truckId = truckId;
        this.truckName = truckName;
      }
}

module.exports = GetTrucksResponse;