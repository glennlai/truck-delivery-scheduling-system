class PostNewDeliveryResponse {
    constructor(deliveryId, deliveryName, truckId, truckName, startDateTime, endDateTime, deliveryStatus) {
        this.deliveryId = deliveryId;
        this.deliveryName = deliveryName;
        this.truckId = truckId;
        this.truckName = truckName;
        this.deliveryStartDateTime = startDateTime;
        this.deliveryEndDateTime = endDateTime;
        this.deliveryStatus = deliveryStatus;
      }
}

module.exports = PostNewDeliveryResponse;