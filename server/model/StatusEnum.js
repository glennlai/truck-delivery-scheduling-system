const Enum = require('enum');

const statusEnum = new Enum(['NEW', 'SCHEDULED', 'COMPLETED', 'CANCELLED'], { ignoreCase: true })

module.exports = statusEnum;
