const express = require('express');
const axios = require('../node_modules/axios');

const router = express.Router();
const truckController = require('../controller/TruckController');
const deliveryController = require('../controller/DeliveryController');

router.get('/trucks', function(req, res) {
    console.log('GET /trucks called');
    return truckController.getTrucks(req, res);
});

router.get('/truck', function(req, res) {
    console.log('GET /truck called');
    return truckController.getSingleTruck(req, res);
});

router.get('/deliveries', function(req, res) {
    console.log('GET /deliveries called');
    return deliveryController.getDeliveries(req, res);
});

router.post('/delivery/new', function(req, res) {
    console.log('POST /delivery/new called');
    return deliveryController.postNewDelivery(req, res);
});

router.patch('/delivery/:deliveryId/status', function(req, res) {
    console.log('PATCH /delivery/:deliveryId/status called');
    return deliveryController.updateStatus(req, res);        
});
// more routes for our API will happen here
module.exports = router;
