const mongoose = require('mongoose');
const TruckSchema = require('../model/entity/Truck')
const GetTrucksResponse = require('../model/response/GetTrucksResponse')

exports.getTrucks = (req, res) => {
    console.log({message: 'in truckService - getTrucks'}); 
    TruckSchema.find({}, function(err, results) {
        if (err){
            console.log(err);            
        }
        else{
            var response = [];
            console.log("results", results);
            results.forEach(e => {
                response.push(new GetTrucksResponse(e.truckId, e.truckName))                          
            });
            res.contentType('application/json');
            res.send(JSON.stringify(response));
        }
    });
};

exports.getSingleTruck = (req, res) => {
    console.log({message: 'in truckService - getTruck'}); 
    const truckId = req.query.truckId ? req.query.truckId: ''; 
    TruckSchema.find({truckId: truckId}, function(err, results) {
        if (err){
            console.log(err);            
        }
        else{
            var response = [];
            console.log("results", results);
            results.forEach(e => {
                response.push(new GetTrucksResponse(e.truckId, e.truckName));    
            })                                 
            res.contentType('application/json');
            res.send(JSON.stringify(response));
        }
    });
};
