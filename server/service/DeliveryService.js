const e = require('express');
const { isNull } = require('util');
const uuidv4 = require('uuidv4');
const DeliverySchema = require('../model/entity/Delivery')
const TruckSchema = require('../model/entity/Truck')
const GetDeliveriesResponse = require('../model/response/GetDeliveriesResponse')
const PostNewDeliveryResponse = require('../model/response/PostNewDeliveryResponse')
const UpdateStatusResponse = require('../model/response/UpdateStatusResponse')

exports.getDeliveries = async (req, res) => {
    console.log({message: 'in deliveryService - getDeliveries'}); 
    DeliverySchema.find({}, function(err, results) {        
        if (err){
            console.log(err);            
        }
        else {
            var response = [];
            // console.log("results", results);
            if (results.length == 0) {
                console.log("no delivery records.")
            }       
             
            results.forEach(e => {               
                response.push(new GetDeliveriesResponse(
                    e.deliveryId, e.deliveryName, 
                    e.truckId,
                    e.address, 
                    e.startDateTime, e.endDateTime, 
                    e.deliveryStatus));                                       
            });
            res.contentType('application/json');
            res.status(200).json(response);
        }
    });
};

exports.postNewDelivery = async (req, res) => {
    if (!req.body.startDateTime) {return res.status(400).json({message: 'Missing startDateTime'});}
    if (!req.body.endDateTime) {return res.status(400).json({message: 'Missing endDateTime'});}

    let truckId = req.body.truckId ? req.body.truckId: ''; 
    let address = req.body.address ? req.body.address: ''; 
    let startDateTime = req.body.startDateTime;
    let endDateTime = req.body.endDateTime
    
    try {        
        // check whether delivery for truck of startDateTime and endDateTime exist
        console.log({truckId: truckId, startDateTime: startDateTime, endDateTime: endDateTime});
        var isExist = (await DeliverySchema.exists({truckId: truckId, startDateTime: startDateTime, endDateTime: endDateTime})).valueOf();
        console.log('isExist: ' + isExist);

        if (!isExist) {
            // create new delivery record
            const newDeliveryRecord = new DeliverySchema({
                deliveryId: uuidv4.uuid(), 
                deliveryName: startDateTime + endDateTime + truckId, 
                address: address,
                startDateTime: startDateTime, 
                endDateTime: endDateTime, 
                deliveryStatus: 'NEW', 
                truckId: truckId
            });
            
            var truckName = '';            
            TruckSchema.findOne({truckId: truckId}, (err, doc) => {
                if (err) {console.log(err)}
                else {
                    truckName = doc.truckName ? doc.truckName: '';
                }
            })

            //save new delivery record
            DeliverySchema.create(newDeliveryRecord, (err, doc) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("Created New Delivery Record.")  // Success
                    return res.status(200)
                    .json(new PostNewDeliveryResponse(
                        doc.deliveryId, doc.deliveryName, doc.truckId, truckName, doc.startDateTime, doc.endDateTime, doc.deliveryStatus));
                }
            });                    
        } else {
            return res.status(500).json(
                {
                    title: 'Clashing delivery schedule',
                    message: {truckId: truckId, startDateTime: startDateTime, endDateTime: endDateTime}
                });  
        }

    } catch (ex) {
        res.status(500).json(
            {
                title: 'Unexpected Error Occurred',
                message: ex
            });
        throw new Error(ex);
    }
};

exports.updateStatus = (req, res) => {
    const status = req.query.status;
    const deliveryId = req.params.deliveryId;
    console.log({message: 'in deliveryService - updateStatus: '}); 

    DeliverySchema.findOne({deliveryId: deliveryId}, (err, doc) => { 
        if (err) { console.log(err); } 
        else {
            switch (doc.deliveryStatus) {
                // when db delivery status = NEW
                case "NEW":
                    // only update when update status is SCHEDULED / CANCELLED
                    if (status == 'SCHEDULED' || status == 'CANCELLED') {
                        DeliverySchema.updateOne({deliveryId: deliveryId}, {$set: { deliveryStatus: status }}, (err, result) => {                        
                            return res.status(200).json(new UpdateStatusResponse(doc.deliveryId, status));
                        })
                    } else {
                        // return BAD REQUEST 400 when update status is NOT SCHEDULED / CANCELLED
                        return res.status(400).json(
                            {
                                title: 'Invalid Status Update',
                                message: 'status: NEW => SCHEDULED/CANCELLED'
                            }
                        );                        
                    }       
                    break;  
                case "SCHEDULED":
                    // only update when update status is COMPLETED / CANCELLED
                    if (status == 'COMPLETED' || status == 'CANCELLED') {
                        DeliverySchema.updateOne({deliveryId: deliveryId}, {$set: { deliveryStatus: status }}, (err, result) => {
                            return res.status(200).json(new UpdateStatusResponse(doc.deliveryId, status));
                        })
                    } else {
                        // return BAD REQUEST 400 when update status is NOT COMPLETED / CANCELLED
                        return res.status(400).json(
                            {
                                title: 'Invalid Status Update',
                                message: 'status: SCHEDULED => COMPLETED/CANCELLED'
                            }
                        );                        
                    }   
                    break;
                default:
                    // return BAD REQUEST 400 when delivery status is already COMPLETED / CANCELLED
                    return res.status(400).json(
                        {
                            title: 'Invalid Status Update',
                            message: 'status: NEW => SCHEDULED/CANCELLED , status: SCHEDULED => COMPLETED/CANCELLED'
                        }   
                    );                    
                        
            }
        }   
    });
}